class Api::V1::QuestionsController < ApplicationController
  before_action :set_question, only: [:show, :update, :destroy]

  def index
    @questions = Question.includes(:role, :mapping).order('created_at DESC').page(params[:page])

    render json: {
      questions: @questions.map{|s| QuestionSerializer.new(s) },
      page: @questions.current_page,
      total_count: Question.count
    }
  end

  def show
    render json: @question
  end

  def create
    @question = Question.new(question_params)

    if @question.save
      render json: @question, status: :created
    else
      render json: @question.errors, status: :unprocessable_entity
    end
  end

  def update
    if @question.update(question_params)
      render json: @question
    else
      render json: @question.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @question.destroy
  end

  def roles
    @roles = Role.all

    render json: @roles
  end

  def mappings
    @mappings = Mapping.all

    render json: @mappings
  end

  def teaming_stages
     render json: {teaming_stages: Question::TEAMING_STAGES}
  end

  def question_types
    render json: {question_types: Question::QUESTION_TYPES}
  end

  def conditions
    render json: { conditions: Question::CONDITIONS}
  end

  private
    def set_question
      @question = Question.find(params[:id])
    end

    def question_params
      params.require(:question).permit(:priority, :question, :teaming_stage, :is_required, :question_type, :condition, :appear, :frequency, :role_id, :mapping_id)
    end
end
