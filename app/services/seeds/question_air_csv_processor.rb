class Seeds::QuestionAirCsvProcessor
	attr_reader :csv_file
	def initialize(filename)
		@csv_file = filename
	end

	def process!
		csv_data = read_csv
		ActiveRecord::Base.transaction do
			csv_data.each do |question|
				execute(question)
			end
		end
	end

	def execute(question_raw)
		priority = question_raw['Pri']
		question = question_raw['Question']
		teaming_stage = question_raw['Teaming Stages']
		appear = question_raw['Appears (Day)']
		frequency = question_raw['Frequency']
		question_type = question_raw['Type']
		role = question_raw['Role']
		is_required = question_raw['Required?']
		condition = question_raw['Conditions']
		mapping = question_raw['Mapping']

		role = Role.find_by(name: role)
		mapping = Mapping.find_or_create_by(name: mapping)
		teaming_stage = teaming_stage&.underscore
		question_type = question_type&.gsub(" ", "_")&.underscore
		condition = condition&.underscore
		is_required = string_to_boolean(is_required)

		Question.create!(
			priority: priority, 
			question: question, 
			teaming_stage: teaming_stage,
			appear: appear,
			frequency: frequency,
			question_type: question_type,
			role: role,
			is_required: is_required,
			condition: condition,
			mapping: mapping
		)
	end


	def string_to_boolean(val)
		return true if val.downcase == "yes"
		return false if val.dowcase == "no"
	end

	def read_csv
		CSV.parse(File.read("#{Rails.root}/uploads/csv_data.csv"), headers: true)
	end
end