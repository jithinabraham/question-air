class Question < ApplicationRecord
  enum team_stage: {norming:'norming', forming: 'forming', performing: 'performing', everyone: 'all'}
  enum question_type: {rating_scale: 'rating_scale'}
  enum condition: {always: 'always',rare: 'rare', medium: 'medium'}
  paginates_per 10

  TEAMING_STAGES = %w[norming forming performing all]
  QUESTION_TYPES = %w[rating_scale]
  CONDITIONS = %w[always rare medium]

  belongs_to :role
  belongs_to :mapping

  validates :teaming_stage, presence: true
  validates :question_type, presence: true
  validates :condition, presence: true
  validates :priority, presence: true, numericality: true
  validates :question, presence: true
  validates :appear, presence: true, numericality: true
  validates :frequency, presence: true, numericality: true

end
