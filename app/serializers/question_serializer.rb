class QuestionSerializer < ActiveModel::Serializer
  attributes :id, :priority, :question, :appear, :frequency, :is_required, :teaming_stage,
             :question_type, :condition, :role, :mapping, :role_id, :mapping_id
  def role
    object.role.name
  end

  def mapping
    object.mapping.name
  end

end
