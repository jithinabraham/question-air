class CreateQuestions < ActiveRecord::Migration[6.0]
  def change
    
    create_table :questions do |t|
      t.integer :priority
      t.text :question
      t.integer :appear
      t.integer :frequency
      t.references :role, null: false, foreign_key: true
      t.boolean :is_required
      t.references :mapping, null: false, foreign_key: true

      t.timestamps
    end
  end
end