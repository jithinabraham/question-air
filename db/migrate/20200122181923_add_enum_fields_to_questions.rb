class AddEnumFieldsToQuestions < ActiveRecord::Migration[6.0]
  def up
    execute <<-SQL
      CREATE TYPE team_stage_types AS ENUM ('norming', 'forming', 'performing', 'all');
      CREATE TYPE question_types AS ENUM ('rating_scale');
      CREATE TYPE condition_types AS ENUM ('always','rare','medium');
    SQL
    
    add_column :questions, :teaming_stage, :team_stage_types
    add_column :questions, :question_type, :question_types
    add_column :questions, :condition, :condition_types
  end

  def dwon
    remove_column :questions, :team_stage
    remove_column :questions, :question_type
    remove_column :questions, :condition
    
    execute <<-SQL
      DROP TYPE team_stage_types
      DROP TYPE question_types
      DROP TYPE condition_types
    SQL
  end
end
