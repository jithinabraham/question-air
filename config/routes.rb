Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :questions do 
        get :roles, on: :collection
        get :mappings, on: :collection
        get :teaming_stages, on: :collection
        get :question_types, on: :collection
        get :conditions, on: :collection
      end
    end
  end
end
