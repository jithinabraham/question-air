# README

* Ruby version - 2.6.3

* Rails version - 6.0.2 

* Database - postgreSQL 

Steps to start the application
1. `RAILS_ENV=env rake db:create db:migrate`
2. Load the question data from the csv `RAILS_ENV=env rake db:seed`
3. Run the application `rails s `
